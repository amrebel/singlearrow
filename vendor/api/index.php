<?php

require 'flight/Flight.php';
error_reporting(0);

function arrayProc($stringIn, $val) {
	if (is_array($val)) {
		$res = '';
		foreach ($val as $i => $val1) {
			$stringOut = $stringIn . '[' . $i . ']';
			$res .= arrayProc($stringOut, $val1);
		}
		return $res;
	} else {
		return $stringIn . "=" . urlencode($val) . '&';
	}
}

function callUrl($Url, $request_array = NULL) {
	$request_url = '';
	if ($request_array != NULL) {
		foreach ($request_array as $key => $val) {
			$request_url.= arrayProc($key, $val);
		}
	}
print_r($request_url);
	// OK cool - then let's create a new cURL resource handle
	$ch = curl_init();

	#$login = "admin:admin"; // Benutzerdaten (Username und Passwort)
	#curl_setopt($ch, CURLOPT_USERPWD, $login); // Speichern Login-Daten in Optionen
	// Now set some options (most are optional)
	// Set URL to download
	curl_setopt($ch, CURLOPT_URL, $Url);

	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $request_url);

	// Set a referer
	# curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
	// User agent
	curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");

	// Include header in result? (0 = yes, 1 = no)
	curl_setopt($ch, CURLOPT_HEADER, 0);

	// Should cURL return or print out the data? (true = return, false = print)
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// Timeout in seconds
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);

	// Download the given URL, and return output
	$output = curl_exec($ch);

	if (curl_errno($ch)) {
		$error = curl_error($ch);
		// Close the cURL resource, and free system resources
		curl_close($ch);
		return $error;
	} else {
		// Close the cURL resource, and free system resources
		curl_close($ch);
		return $output;
	}
}

function getChildNode(DOMNode $parent) {
	$child = $parent->firstChild;
	if ($child->nodeType == 3) {
		$sibling = $child->nextSibling;
		if ($sibling->nodeType == 3) {
			return $sibling->nextSibling;
		} else {
			return $sibling;
		}
	} else {
		return $child;
	}
}

Flight::route('/projects', function() {
	
	$aProjects = array();
	$oProject = new stdClass();
	$oProject->id = 1;
	$oProject->name = 'Pipeline';
	$oProject->description = 'Pipeline is the next big thing at charity: water. It’s a system of local leaders, innovative technology and trained mechanics all working together to keep water flowing at charity: water projects around the world.';
	$oProject->url = 'https://www.betterplace.org/de/projects/8081-das-letzte-siebte-leben-hilfe-fur-svetlana';
	$oProject->image = 'img/bookle-supported-charity-1.jpg';
	$oProject->amount = 30.45;
	$aProjects[] = $oProject;
	
	$oProject = new stdClass();
	$oProject->id = 2;
	$oProject->name = 'The Book Bus';
	$oProject->description = 'The Book Bus aims to improve child literacy rates in Africa, Asia and South America by providing children with books and the inspiration to read them. ';
	$oProject->url = 'https://www.betterplace.org/de/projects/8081-das-letzte-siebte-leben-hilfe-fur-svetlana';
	$oProject->image = 'img/bookle-supported-charity-2.jpg';
	$oProject->amount = 30.45;
	$aProjects[] = $oProject;
	
	$oProject = new stdClass();
	$oProject->id = 3;
	$oProject->name = 'African Library Project';
	$oProject->description = 'The African Library Project coordinates book drives in the United States and partners with African schools and villages to start small libraries.';
	$oProject->url = 'https://www.betterplace.org/de/projects/8081-das-letzte-siebte-leben-hilfe-fur-svetlana';
	$oProject->image = 'img/bookle-supported-charity-3.jpg';
	$oProject->amount = 30.45;
	$aProjects[] = $oProject;
	
	echo(json_encode($aProjects));
});

// for main content

Flight::route('/maincontent', function() {
	$aPayments = array();
	$config = parse_ini_file(realPath('website-contents/mainContent.ini'), TRUE);
	echo(json_encode($config));
});

// for About Us

Flight::route('/ourteam', function() {
	$aPayments = array();
	$config = parse_ini_file(realPath('website-contents/ourTeam.ini'), TRUE);
	echo(json_encode($config));
});

// for About Us

Flight::route('/ourservices', function() {
	$aPayments = array();
	$config = parse_ini_file(realPath('website-contents/ourServices.ini'), TRUE);
	echo(json_encode($config));
});

Flight::route('/payments', function() {
	$aPayments = array();
	$oPayment = new stdClass();
	$oPayment->id = 'debitnote';
	$oPayment->name = 'Sepa-Lastschrift';
	$oPayment->description = 'Hier bezahlen sie bequem per SEPA-Lastschrift. Geben Sie Ihre Iban und Bic an und wir ziehen den Betrag bequem dort ein.';
	$oPayment->needAccount = true;
	$aPayments[] = $oPayment;
	
	$oPayment = new stdClass();
	$oPayment->id = 'payadvance';
	$oPayment->name = 'Vorauskasse';
	$oPayment->description = 'Hier bezahlen Sie per Vorkassen-Überweisung.';
	$oPayment->needAccount = false;
	$aPayments[] = $oPayment;
	echo(json_encode($aPayments));
});

Flight::route('/books', function() {
	$search = $_GET['Search'];
	$search = str_replace('+','%20',urlencode($search));
	$request_array['hdnCurrency'] = 'EUR';
	$request_array['hdnConvRate'] = 1;
	$request_array['shopUrl'] = 'http%3A%2F%2Fwww.aha-buch.de%2F';
	$request_array['Code'] = 'rebecca+h%C3%BCter';
	$request_array['x'] = 35;
	$request_array['y'] = 31;
	
	/*#$request_array['__VIEWSTATE'] = '/wEPDwUKLTU0Nzg0MjM1M2RkD9Iflo BhG3yTvdmZxUJ5qUYS0Oa8zHZbIWVt5H pmY=';
	#$request_array['__EVENTVALIDATION'] = '/wEWFwLdy8nMAwL0lKWcBwKp5oOFCgKT58bbBwKbjZG9BAKi ZzpCwKrhccIAvSshIUIApXWh40FArnCjeYKAuKUhc4LAqnt9zECxICPhQICj5bFyQ8CpICdygsC5de3nQ0CjLHx0AsC74iFpQICzbmOvAICi4GatgoCrrrIggwCutaivQgC6bn/rA0jPcykWjDMGfcKoEiEGYSU/UqnFmg1ufQavkNDiJH4Sw==';
	$request_array['hdnsearchtext'] = $search;
	$request_array['hdnSearchKeyWord'] = $search;
	$request_array['hdnPageGroup'] = 1;
	$request_array['hdnpagenumber'] = 1;
	$request_array['hdnbooktype'] = 'New';
	$request_array['searchtype'] = 'domainsearch';*/
	
	$test = callUrl('http://www.aha-buch.de/search/Domainsearch/new/'.$search);

	$doc = new DOMDocument();
	$html = utf8_decode($test);

	$doc->loadHTML($html);

	$contents = array();
	$params = $doc->getElementsByTagName('div');
	foreach ($params AS $param) {
		if ($param->hasAttribute('class') && $param->getAttribute('class') == 'sboxwrapper sresults clearfix') {
			$content = new stdClass();

			$sContainer = $doc->saveHTML($param);
			$oContainer = new DOMDocument();
			$oContainer->loadHTML($sContainer);

			$oContainerParamsDiv = $oContainer->getElementsByTagName('div');
			$oContainerParamsH2 = $oContainer->getElementsByTagName('h2');
			$oContainerParamsSpan = $oContainer->getElementsByTagName('span');
			$oContainerParamsA = $oContainer->getElementsByTagName('a');

			// title
			$content->title = utf8_decode(trim($oContainerParamsH2->item(0)->nodeValue));
			
			foreach ($oContainerParamsA AS $aLink) {
				if ($aLink->hasAttribute('onclick') && $aLink->getAttribute('title') == 'In den Warenkorb') {
					$link = $aLink->getAttribute('onclick');
					$link = explode("','",$link);
					$content->id = str_replace("')","",$link[1]);
				}
			}
			
			
			
			// price
			foreach ($oContainerParamsSpan AS $param2) {
				if ($param2->hasAttribute('class') && $param2->getAttribute('class') == 'currvalue spPrice') {
					$sFEuroPrice = $param2->nodeValue;
					$sEuroPrice = str_replace(',', '.', $sFEuroPrice);
					$sPrice = str_replace('EUR','', $sEuroPrice);
					$content->price = trim($sPrice);
				}
			}

			foreach ($oContainerParamsDiv AS $param2) {
				if ($param2->hasAttribute('class') && $param2->getAttribute('class') == 'sfred pt') {
					$content->stock = trim($param2->nodeValue);
				}
				if ($param2->hasAttribute('class') && $param2->getAttribute('class') == 'publisher') {
					$content->publisher = trim(utf8_decode(getChildNode($param2)->nodeValue));
				}
				if ($param2->hasAttribute('class') && $param2->getAttribute('class') == 'author') {
					$content->author = trim(utf8_decode(getChildNode($param2)->nodeValue));
				}
				if ($param2->hasAttribute('class') && $param2->getAttribute('class') == 'left_thumb') {
					$content->thumb = trim(utf8_decode(getChildNode(getChildNode($param2))->getAttribute('src')));
				}
				if ($param2->hasAttribute('class') && $param2->getAttribute('class') == 'damcond') {
					$content->type = trim(utf8_decode($param2->nodeValue));
				}
				if ($param2->hasAttribute('class') && $param2->getAttribute('class') == 'itemicon clearfix') {
					$content->media = trim(utf8_decode($param2->nodeValue));
				}
			}
			$contents[] = $content;
		}
	}
	echo(json_encode($contents));
});

Flight::start();
?>
