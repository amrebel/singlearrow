singleArrow
===========

This is a One Page website. The core technologies used here are:

AngularJS skeleton: Angular Seed (https://github.com/angular/angular-seed)

CSS Framework: Twitter Bootstrap (3.0)

CSS Preprocessor: LESS

Map: Leaflet



It is live here:

http://alokpant.com/demo/singleArrow/#/v1
