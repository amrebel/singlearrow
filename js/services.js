'use strict';

/* Services


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', []).
  value('version', '0.1');
 */
 //taken from http://jsfiddle.net/alansouzati/6L7tA/
  
var $scope, $location;
nosfairApp.service('anchorSmoothScroll', function ($document, $window) {

	var document = $document[0];
	var window = $window;

	function getCurrentPagePosition(window, document) {
		// Firefox, Chrome, Opera, Safari
		if (window.pageYOffset) return window.pageYOffset;
		// Internet Explorer 6 - standards mode
		if (document.documentElement && document.documentElement.scrollTop)
			return document.documentElement.scrollTop;
		// Internet Explorer 6, 7 and 8
		if (document.body.scrollTop) return document.body.scrollTop;
		return 0;
	}

	function getElementY(document, element) {
		var y = element.offsetTop - 150;
		var node = element;
		while (node.offsetParent && node.offsetParent != document.body) {
			node = node.offsetParent;
			y += (node.offsetTop);
		}
		return y;
	}

	this.scrollDown = function (startY, stopY, speed, distance) {

		var timer = 0;

		var step = Math.round(distance / 25);
		var leapY = startY + step;

		for (var i = startY; i < stopY; i += step) {
			setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
			leapY += step;
			if (leapY > stopY) leapY = stopY;
			timer++;
		}
	};

	this.scrollUp = function (startY, stopY, speed, distance) {

		var timer = 0;

		var step = Math.round(distance / 25);
		var leapY = startY - step;

		for (var i = startY; i > stopY; i -= step) {
			setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
			leapY -= step;
			if (leapY < stopY) leapY = stopY;
			timer++;
		}
	};

	this.scrollToTop = function (stopY) {
		scrollTo(0, stopY);
	};

	this.scrollTo = function (elementId, speed) {
		// This scrolling function
		// is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

		var element = document.getElementById(elementId);

		if (element) {
			var startY = getCurrentPagePosition(window, document);
			var stopY = getElementY(document, element);

			var distance = stopY > startY ? stopY - startY : startY - stopY;

			if (distance < 100) {
				this.scrollToTop(stopY);

			} else {

				var defaultSpeed = Math.round(distance / 100);
				speed = speed || (defaultSpeed > 20 ? 20 : defaultSpeed);

				if (stopY > startY) {
					this.scrollDown(startY, stopY, speed, distance);
				} else {
					this.scrollUp(startY, stopY, speed, distance);
				}
			}

		}

	};

});

/* ====================================

		OUR SERVICES API CALL

==================================== */

nosfairApp.factory('OurServicesSliderText', function ($http, $q) {
	var bodyText = {};
	var deferred = $q.defer();
	var data = [];
	var pushedText = {};
	
	bodyText.getText = function () {
		$http.get('vendor/api/ourservices').success(function (d) {			
			angular.forEach(d, function(value, key) {
				this.push(value);
			}, data);
		
			deferred.resolve();
		});
		return deferred.promise;
	};
	
	bodyText.data = function () { return data; };
	return bodyText;
});

/* ====================================

		ABOUT US API CALL

==================================== */

nosfairApp.factory('OurTeamSliderText', function ($http, $q) {
	var bodyText = {};
	var deferred = $q.defer();
	var data = [];
	var pushedText = {};
	
	bodyText.getText = function () {
		$http.get('vendor/api/ourteam').success(function (d) {			
			angular.forEach(d, function(value, key) {
				this.push(value);
			}, data);
		
			deferred.resolve();
		});
		return deferred.promise;
	};
	
	bodyText.data = function () { return data; };
	return bodyText;
});

/* ====================================

		WHOLE WEBSITE API CALL

==================================== */

nosfairApp.factory('MainContentTextRetrieve', function ($http, $q) {
	var bodyText = {};
	var deferred = $q.defer();
	var data = [];
	
	bodyText.getText = function () {
		//console.log('inside');
		$http.get('vendor/api/maincontent').success(function (d) {
			data = d;
			//console.log(d);
			//console.log("inside");
			deferred.resolve();
		});
		return deferred.promise;
	};
	
	bodyText.data = function () { return data; };
	return bodyText;
});

/* ====================================

		CONTACT US EMAIL

==================================== */

nosfairApp.factory('PushEmail', function ($http, $q) {
	var contactUsForm = {};
	var deferred = $q.defer();
	var data = [];
	
	contactUsForm.pushData = function (cntFormValues) {
		console.log(cntFormValues);
		$http.post('mailScript.php', cntFormValues).success(function (d) {
			data = d;
			console.log(d);
			deferred.resolve();
		}).error(function (d) {
			data = d;
			console.log(d);
			deferred.resolve();
		});
		return deferred.promise;
	};
	
	contactUsForm.data = function () { return data; };
	return contactUsForm;
});