'use strict';

nosfairApp.controller('onePageApp', ['$scope', '$http', '$location', '$sce', '$interval', 'MainContentTextRetrieve', 'anchorSmoothScroll', 'OurServicesSliderText', 'OurTeamSliderText', 'PushEmail', function ($scope, $http, $location, $sce, $interval, MainContentTextRetrieve, anchorSmoothScroll, OurServicesSliderText, OurTeamSliderText, PushEmail) {	

	var val = ""; //flag for showing hidden divs
			
	//initialize the home map
	angular.extend($scope, {
		home: {
			lat: 52.544652,
			lng: 13.356049,
			zoom: 16
		},
		
		homeMarkers: {
			main_marker: {
				lat: 52.544652,
			lng: 13.356049,
				focus: true,
				//message: "Hey, drag me if you want",
				title: "Standort Home",
				draggable: false
			}
		},
		
		homeDefaults: {
            scrollWheelZoom: false
        }
	});
	
	$scope.isActive = function (viewLocation) {
		var getLocation;
		if(!$location.hash()) {
			getLocation = "/start";	
		} else {
			getLocation = "/"+$location.hash();	
		}
		var active = (viewLocation === getLocation);

		return active;
	};

	
	//fix the scrolling component
	$scope.gotoElement = function (eID){
      // set the location.hash to the id of
      // the element you wish to scroll to.
		$location.hash(eID);
		anchorSmoothScroll.scrollTo(eID);
    };
	
	//carousal component
	
	//Get contents for the slider
	$scope.slides = [];
	OurServicesSliderText.getText().then(function () {
		var x = OurServicesSliderText.data();
		for(var i in x) {
			$scope.slides.push(x[i]);
		};
	});
	
	$scope.myInterval = 5000;
	
	//get site contents
	
	MainContentTextRetrieve.getText().then(function () {
		var textList = MainContentTextRetrieve.data();
		
		//$scope.ourServices = $sce.trustAsHtml(textList.Start.Subline); //sub description
		
		//Cloud Text
		$scope.headerText = $sce.trustAsHtml(textList.HeaderText.Text);
		$scope.goToTop = $sce.trustAsHtml(textList.HeaderText.GoToTop);
		
		//ourServices
		$scope.ourServicesTitle = textList.OurServices.Title; //main heading
		$scope.ourServicesSubHeading = textList.OurServices.SubHeading;
		$scope.ourServicesTextDescription = textList.OurServices.TextDescription;
		
		//ProgrammingFramework Section
		$scope.programmingFrameworkHeading = textList.ProgrammingFrameworks.Title;
		$scope.programmingFrameworkTextDescription = textList.ProgrammingFrameworks.TextDescription;
		
		//Philosophie
		$scope.philosophyTitle = textList.Philosophy.Title;
		$scope.philosophyTextDescription = textList.Philosophy.TextDescription;
		$scope.imageHeading = textList.Philosophy.ImageHeading;
		$scope.imageTextDescription = textList.Philosophy.ImageTextDescription;
		$scope.balanceTitle = textList.Philosophy.BalanceTitle;
		$scope.balanceDescription = textList.Philosophy.BalanceDescription;
		
		//Philoshophy Contact
		$scope.philosophyContactTitle = textList.PhilosophyContact.Title;
		$scope.berlinContactTitle = textList.PhilosophyContact.BerlinContactTitle;
		$scope.philosophyBerlinContactAddress = $sce.trustAsHtml(textList.PhilosophyContact.BerlinAddress);
		$scope.thedinghausenContactTitle = textList.PhilosophyContact.ThedinghausenContactTitle;
		$scope.philosophyThedinghausenContactAddress = $sce.trustAsHtml(textList.PhilosophyContact.BerlinAddress);
		$scope.philosophyContactLink = textList.PhilosophyContact.Link;
		$scope.philosophyContactEmail = textList.PhilosophyContact.Email;
		$scope.philosophyContactTelephone = textList.PhilosophyContact.Telephone;
		
		//About Us
		$scope.aboutUsTitle = textList.AboutUs.Title;
		$scope.aboutUsSubHeading = textList.AboutUs.SubHeading;
		$scope.aboutUsTextDescription = textList.AboutUs.TextDescription;
		
		//Career
		$scope.careerTitle = textList.Career.Title;
		$scope.careerTextDescription = textList.Career.TextDescription;
		$scope.quoteText = textList.Career.QuoteText;
		$scope.quoteAuthor = textList.Career.QuoteAuthor;
		$scope.linkDescription = textList.Career.LinkDescription;
		$scope.imageLink = textList.Career.ImageLink;
		$scope.imageLocation = textList.Career.ImageLocation;
		$scope.imageAlt = textList.Career.imageAlt;
		
		//Contact Us
		$scope.contactUsTitle = textList.ContactUs.Title;
		$scope.contactUsDescription = textList.ContactUs.TextDescription;
		$scope.contactUsButtonText = textList.ContactUs.ButtonText;
		
		//Footer Section
		$scope.currentDate = Date.now();
		
		//AGB
		$scope.agbTitle = textList.Agb.Title;
		$scope.agbTextDescription = $sce.trustAsHtml(textList.Agb.Text);
		
		//Imprint
		$scope.impressumTitle = textList.Imprint.Title;
		$scope.impressumTextDescription = $sce.trustAsHtml(textList.Imprint.Text);
		
		//Thank you
		$scope.thankYouTitle = textList.ThankYou.Title;
	});
	
	//Get contents for the slider
	$scope.ourTeam = [];
	OurTeamSliderText.getText().then(function () {
		var x = OurTeamSliderText.data();
		for(var i in x) {
			$scope.ourTeam.push(x[i]);
		};
	});
	
	var contactFlag = 0; //thank you page flag
	var defaultForm = {
		companyName : "",
		vorname : "",
		nachname : "",
		telefonnummer : "",
		email : "",
		message : ""
	};
	
	$scope.user = [];
	
	$scope.sendEmail = function (user) {
		//$scope.userForm.$setPristine(true); //clear the state
		$scope.user = []; //empty the form
		val = ""; //remove the message if there is any
		var telefon = 0;
		var companyName = 'no name given';
		var message = 'no message';
		
		//correct the values - AngularJS automatically trims the field values
		if(user.companyName !== '' && user.companyName !== undefined && user.companyName !== null) {
			companyName = user.companyName;
		}
	
		if(user.telefonnummer !== '' && user.telefonnummer !== undefined && user.telefonnummer !== null) {
			telefon = user.telefonnummer;
		}
		
		if(user.message !== '' && user.message !== undefined && user.message !== null) {
			message = user.message;
		}
		
		var emailArray = { 
			'subject': 'Contact Request',
			'CompanyName': companyName,
			'Vorname': user.vorname,
			'Nachname': user.nachname,
			'Telefonnummer': telefon,
			'Email': user.email,
			'Message': message,
		};
		
		var fieldsToPush = JSON.stringify(emailArray);
		
		PushEmail.pushData(fieldsToPush).then(function () {
			/*
				$scope.userForm.$setPristine();
			
				$scope.user.companyName.$setPristine();
				$scope.user.vorname.$setPristine();
				$scope.user.nachname.$setPristine();
				$scope.user.telefonnummer.$setPristine();
				$scope.user.email.$setPristine();
				$scope.user.message.$setPristine();
			*/
			var message = PushEmail.data();

			console.log(message);
			
			if(message.error == "") {
				$scope.thankYouTitleTextDescription = $sce.trustAsHtml(message.success);
				console.log(message.success);
				val = "success";
			}
			
			if(message.success == "") {
				$scope.thankYouTitleTextDescription = $sce.trustAsHtml(message.error);
				val = "error";
				$scope.user = user;
			}
		});
		
		console.log(val);
		$scope.isSuccessOrError = function (pid) {
			return (val == pid) ? true : false;
		}
	};
	
	$scope.showHiddenDiv = function (numb) {
		val = (numb == 1) ? 'agb' : 'impressum';
		$location.hash(val);
		anchorSmoothScroll.scrollTo(val);
	};
	
	$scope.isShown = function (pid) {
		return (val == pid) ? true : false;
	};
	
	$scope.$watch('userForm', function (newV, oldV) {
		if(newV != oldV) {
			$scope.isSuccessOrError = function (pid) {
				return (val == pid) ? true : false;
			}
		}
	});
	
	/* ==============================================
	
			CHECK FOR PREVIOUS CLICKED MENU
			
	=============================================== */
	
	$location.url($location.hash()); //clean the url
	
	
	/* ==============================================
	
				FRAMEWORK ICON LIST
			
	=============================================== */
	
	$scope.frameworkListArry = [
		{
			source	: 'img/framework-icons/angularjs-icon.jpg',
			title	: 'AngularJS'			
		},
		
		{
			source	: 'img/framework-icons/apache-ant-icon.jpg',
			title	: 'Apache ant'			
		},
		
		{
			source	: 'img/framework-icons/css3.jpg',
			title	: 'CSS3'			
		},
		
		{
			source	: 'img/framework-icons/datev-icon.jpg',
			title	: 'datev'			
		},
		
		{
			source	: 'img/framework-icons/elasticsearch-icon.jpg',
			title	: 'Elastic Search'			
		},
		
		{
			source	: 'img/framework-icons/git-icon.jpg',
			title	: 'Git'			
		},
		
		{
			source	: 'img/framework-icons/groovy-icon.jpg',
			title	: 'Groovy'			
		},
		
		{
			source	: 'img/framework-icons/html5-icon.jpg',
			title	: 'html5'			
		},
		
		{
			source	: 'img/framework-icons/intrexx-icon.jpg',
			title	: 'intrexx'			
		},
		
		{
			source	: 'img/framework-icons/java-icon.jpg',
			title	: 'Java'			
		},
		
		{
			source	: 'img/framework-icons/jquery-icon.jpg',
			title	: 'jQuery'			
		},
		
		{
			source	: 'img/framework-icons/less-icon.jpg',
			title	: 'Less'			
		},
		
		{
			source	: 'img/framework-icons/mssql-icon.jpg',
			title	: 'MsSQL'			
		},
		
		{
			source	: 'img/framework-icons/mysql-icon.jpg',
			title	: 'MySQL'
			
		},
		
		{
			source	: 'img/framework-icons/oxid-icon.jpg',
			title	: 'Oxid'			
		},
		
		{
			source	: 'img/framework-icons/php-icon.jpg',
			title	: 'PHP'			
		},
		
		{
			source	: 'img/framework-icons/postgresql-icon.jpg',
			title	: 'PostgreSQL'			
		},
		
		{
			source	: 'img/framework-icons/shopware-icon.jpg',
			title	: 'Shopware'
		},
		
		{
			source	: 'img/framework-icons/symfony-icon.jpg',
			title	: 'Symfony'			
		}
	];
	
	$scope.showFrameworksNumber = 4; //to display 4 squares
	
	var m = 0;
	
	var previousLoadedArray = [];
	
	$interval( function () {
		$scope.frameWorkRepeater = [];
		$scope.frameWorkRepeater = getRandomImage();
		angular.copy($scope.frameworkRepeater, previousLoadedArray);
	}, 5000);

	function getRandomImage() {
		var i =0;
		var t = 0;
		var arrayPicture = [];
		var indexArray = [];
		var imageCount = $scope.frameworkListArry.length;
		while (i < $scope.showFrameworksNumber) {
			var index = Math.floor(
				(Math.random() * imageCount * 2) % imageCount
			);
			
			for (var m = 0; m< arrayPicture.length; m++) {
				if(arrayPicture[m] == $scope.frameworkListArry[index]) {
					t = 1;
					continue;
				}
			}
			
			if (t == 0) {
				arrayPicture.push($scope.frameworkListArry[index]);
				i= i + 1;
			} else {
				t = 0;
			}
		}
		return arrayPicture;
	}
	
}]);
