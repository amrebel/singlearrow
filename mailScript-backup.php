<?php
header('Content-Type: application/json');

//
require_once "Mail.php";

/*
 * Initialize 
 * */
$to = "nosfair Homepage <alok@nosfair.com>";
$host = "nosfair.com";
$username = "devel@nosfair.com";
$password = "Pankstrasse8-10Berlin";
$params = (array)json_decode(file_get_contents("php://input"));
$response = new stdClass();
$response->error = "";
$response->success = "";

$defaultSubject = "kein Betreff";
$keyList = array("subject", "Email", "CompanyName", "Message", "Nachname", "Telefonnummer", "Vorname");
$requiedKeys = array("Email", "Message", "Nachname", "Vorname");

function escapeValues($params) {
	$return = array();
	if (is_array($params)) {
		foreach ($params as $key => $value) {
			$return[$key] = escapeValues($value);
		}
	} else {
		return clean($params);
	}
	return $return;
}

function getParam($key, $default = NULL) {
	global $params;
	if (!isset($params[$key]) ||
			$params[$key] === "") {
		return $default;
	} else {
		return escapeValues($params[$key]);
	}
}

function clean($data) {
	$data = trim(stripslashes(strip_tags(htmlspecialchars($data))));
	return $data;
}

function isBot() {
	$bots = array("Indy", "Blaiz", "Java", "libwww-perl", "Python", "OutfoxBot", "User-Agent", "PycURL", "AlphaServer", "T8Abot", "Syntryx", "WinHttp", "WebBandit", "nicebot", "Teoma", "alexa", "froogle", "inktomi", "looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory", "Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot", "crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz");

	foreach ($bots as $bot)
		if (stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false)
			return true;

	if (empty($_SERVER['HTTP_USER_AGENT']) || $_SERVER['HTTP_USER_AGENT'] == " ")
		return true;

	return false;
}

function resolveName($values) {
	return empty($values['Company']) ? $values['Vorname'] . $values['Nachname'] : $values['Nachname'] . "({$values['Vorname']})";
}

function check($list) {
	$badwords = array("adult", "beastial", "bestial", "blowjob", "clit", "cum", "cunilingus", "cunillingus", "cunnilingus", "cunt", "ejaculate", "fag", "felatio", "fellatio", "fuck", "fuk", "fuks", "gangbang", "gangbanged", "gangbangs", "hotsex", "hardcode", "jism", "jiz", "orgasim", "orgasims", "orgasm", "orgasms", "phonesex", "phuk", "phuq", "pussies", "pussy", "spunk", "xxx", "viagra", "phentermine", "tramadol", "adipex", "advai", "alprazolam", "ambien", "ambian", "amoxicillin", "antivert", "blackjack", "backgammon", "texas", "holdem", "poker", "carisoprodol", "ciara", "ciprofloxacin", "debt", "dating", "porn", "link=", "voyeur", "content-type", "bcc:", "cc:", "document.cookie", "onclick", "onload", "javascript");

	foreach ($badwords as $word) {
		global $response;
		foreach ($list as $value) {
			if (strpos(strtolower($value), $word) !== false) {
				$response->error .= "bad word is used.";
				return FALSE;
			}
		}
	}
	return true;
}

/**
 * fetch params from Request
 */
foreach ($keyList as $value) {
	$values[$value] = getParam($value);
}
$error = false;
foreach ($requiedKeys as $value) {
	if (!isset($values[$value])) {
		$response->error .= "$value isn't set.\n";
		$error = true;
	}
}

if (isBot() !== false) {
	$response->error .= "No bots please! UA reported as: " . $_SERVER['HTTP_USER_AGENT'];
	$error = true;
}

if (!$error && $_SERVER['REQUEST_METHOD'] == "POST" && check($values)) {
	$message = "<h1>".$values['Message']."</h1>";
	$message .= "\r\n";
	$message .= 'IP: ' . $_SERVER['REMOTE_ADDR'] . "\r\n";
	$message .= 'Browser: ' . $_SERVER['HTTP_USER_AGENT'] . "\r\n";

	$subject = isset($values['Subject']) ? $values['Subject'] : $defaultSubject;
	$name = resolveName($values);
	$from = "$name <{$values['Email']}>";

	$headers = array(
		'From' => $from,
		'To' => $to,
		'Subject' => $subject
	);
	$smtp = Mail::factory('smtp', array('host' => $host,
				'auth' => true,
				'username' => $username,
				'password' => $password));

	$mail = $smtp->send($to, $headers, $message);

	if (!PEAR::isError($mail)) {
		$response->success = "Mail is successfully sent.";
	} else {
		$response->error .= 'Your mail could not be sent this time.';
	}
}

echo json_encode($response);
exit();
?>