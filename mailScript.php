<?php
header('Content-Type: application/json');
error_reporting(0);
//
require_once "Mail.php";

/*
 * Initialize 
 * */
$to = "nosfair Homepage <alok@nosfair.com>";
$host = "nosfair.com";
$username = "devel@nosfair.com";
$password = "Pankstrasse8-10Berlin";
$params = (array)json_decode(file_get_contents("php://input"));
$response = new stdClass();
$response->error = "";
$response->success = "";

$defaultSubject = "Message via Nosfair Contactform";
$keyList = array("subject", "Email", "CompanyName", "Message", "Nachname", "Telefonnummer", "Vorname");
$requiedKeys = array("Email", "Message", "Nachname", "Vorname");

function escapeValues($params) {
	$return = array();
	if (is_array($params)) {
		foreach ($params as $key => $value) {
			$return[$key] = escapeValues($value);
		}
	} else {
		return clean($params);
	}
	return $return;
}

function getParam($key, $default = NULL) {
	global $params;
	if (!isset($params[$key]) ||
			$params[$key] === "") {
		return $default;
	} else {
		return escapeValues($params[$key]);
	}
}

function clean($data) {
	$data = trim(stripslashes(strip_tags(htmlspecialchars($data))));
	return $data;
}

function isBot() {
	$bots = array("Indy", "Blaiz", "Java", "libwww-perl", "Python", "OutfoxBot", "User-Agent", "PycURL", "AlphaServer", "T8Abot", "Syntryx", "WinHttp", "WebBandit", "nicebot", "Teoma", "alexa", "froogle", "inktomi", "looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory", "Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot", "crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz");

	foreach ($bots as $bot)
		if (stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false)
			return true;

	if (empty($_SERVER['HTTP_USER_AGENT']) || $_SERVER['HTTP_USER_AGENT'] == " ")
		return true;

	return false;
}

function resolveName($values) {
	return empty($values['Company']) ? $values['Vorname'] . $values['Nachname'] : $values['Nachname'] . "({$values['Vorname']})";
}

function check($list) {
	$badwords = array("adult", "beastial", "bestial", "blowjob", "clit", "cum", "cunilingus", "cunillingus", "cunnilingus", "cunt", "ejaculate", "fag", "felatio", "fellatio", "fuck", "fuk", "fuks", "gangbang", "gangbanged", "gangbangs", "hotsex", "hardcode", "jism", "jiz", "orgasim", "orgasims", "orgasm", "orgasms", "phonesex", "phuk", "phuq", "pussies", "pussy", "spunk", "xxx", "viagra", "phentermine", "tramadol", "adipex", "advai", "alprazolam", "ambien", "ambian", "amoxicillin", "antivert", "blackjack", "backgammon", "texas", "holdem", "poker", "carisoprodol", "ciara", "ciprofloxacin", "debt", "dating", "porn", "link=", "voyeur", "content-type", "bcc:", "cc:", "document.cookie", "onclick", "onload", "javascript");

	foreach ($badwords as $word) {
		global $response;
		foreach ($list as $value) {
			if (strpos(strtolower($value), $word) !== false) {
				$response->error .= "bad word is used.";
				return FALSE;
			}
		}
	}
	return true;
}

/**
 * fetch params from Request
 */
foreach ($keyList as $value) {
	$values[$value] = getParam($value);
}
$error = false;
foreach ($requiedKeys as $value) {
	if (!isset($values[$value])) {
		$response->error .= "$value isn't set.\n";
		$error = true;
	}
}

if (isBot() !== false) {
	$response->error .= "No bots please! UA reported as: " . $_SERVER['HTTP_USER_AGENT'];
	$error = true;
}

if (!$error && $_SERVER['REQUEST_METHOD'] == "POST" && check($values)) {
	$message = "<style>
		#nosfairMail {
			width: 600px;
			margin: 20px;
			font-size: 0.7rem;
			text-align: left;
			line-height: 1.5;
			font-family: 'Open Sans', Arial, sans-serif;
			color: #3f4949;
		}

		#nosfairMail h1 {
			font-size: 0.7rem;
			font-weight: normal;
			text-transform: uppercase;
			padding: 5px 0 0px 0;
			margin: 0;
			border-bottom: 1px solid #313131;
			display: inline;
		}

		#nosfairMail span {
			color: #e68633;
			font-weight: bold;
		}

		#nosfairMail a {
			color: #e68633;
		}

		/* Header Part of Top Header */

			#nosfairMail > thead {
				padding-bottom: 5px;
				border-bottom: 1px solid #313131;
				display: block;
				width: 100%;
			}
			
			#nosfairMail > thead > tr:first-child{
				width: 100%;
				display: inline-table;
				padding-bottom: 10px;
				border-bottom: 1px solid #eeeeee;
			}
			
			#nosfairMail > thead > tr:nth-child(2){
				padding-top: 10px;
				display: table-cell;
			}

			#nosfairMail > thead > tr > td{
				height: 60px;
				width: 250px;
				vertical-align: top;
			}

			/* fix height of punch line */
			#nosfairMail > thead > tr:last-child > td{
				height: 20px;
				vertical-align: top;
			}

			/* fix width of Nosfair Logo container */
			#nosfairMail > thead tr td:first-child {
				width: 100px;
			}

			#nosfairMail thead tr a { /* nosfair Link */
				display: inline-block;
			}

			#nosfairMail thead tr a img { /* nosfair Image */
				width: 90px;
				height: auto;
			}

			/* position the address to bottom */

			#nosfairMail > thead tr > td:last-child {
				vertical-align: middle;
			}

		/* Body Part of Top Table */

			#nosfairMail > tbody > tr > td {
				width: 100%;
				display: inline-block;
			}

			#nosfairMail > tbody > tr:not(:first-child) {
				padding: 5px 0 0 0;
				display: block;
			}

			/* Make horizontal line */
			
			#nosfairMail > tbody > tr:first-child > td {
				padding-bottom: 5px;
				border-bottom: 1px solid #313131;
			}

	</style>
	<table id='nosfairMail'>
		<thead>
			<tr>
				<td colspan='2'>
					<p><b>Dies ist eine automatisch generierte E-Mail. Die Absenderadresse dieser E-Mail ist nur zum Versand, nicht zum Empfang von Nachrichten eingerichtet.</b></p>
				</td>
			</tr>
			<tr>
				<td>
					<a href='www.nosfair.com'>
						<img src='http://nosfair.com/img/logo.png' alt='Nosfair Logo'>
					</a>
				</td>
				<td>
					Nosfair<br>
					Haus 1/2 R. 1340, Bouchéstrasse 12<br>
					12435 Berlin, Germany<br>
					info@nosfair.com<br>
					+49 30 532 18 111
				</td>
			</tr>
			<tr>
				<td colspan='2'>wir denken weiter</p>
			</tr>
		</thead>
		
		<tbody>
			
			<tr>
				<td>
					<p>An email has just been sent from Nosfair contact form. Following information was mentioned in the form:</p>
					<h1>Company Name:</h1>
					<p>".$values['CompanyName']."</p>
					
					<h1>Name:</h1>
					<p>".$values['Vorname'].$values['Nachname']."</p>
					
					<h1>Telephone Number:</h1>
					<p>".$values['Telefonnummer']."</p>
					
					<h1>Email:</h1>
					<p>".$values['Email']."</p>
					
					<h1>Message:</h1>
					<p>".$values['Message']."</p>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td>
					<p>Additional information about the user is:
					<table>
						<tr>
							<td>
								IP: 
							</td>
							<td>
								".$_SERVER['REMOTE_ADDR']."
							</td>
						</tr>
						<tr>
							<td>
								Browser: 
							</td>
							<td>
								".$_SERVER['HTTP_USER_AGENT']."
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<p><b>Dies ist eine automatisch generierte E-Mail. Die Absenderadresse dieser E-Mail ist nur zum Versand, nicht zum Empfang von Nachrichten eingerichtet.</b></p>
				</td>
			</tr>
		</tfoot>
	</table>";

	$subject = isset($values['Subject']) ? $values['Subject'] : $defaultSubject;
	$name = resolveName($values);
	$from = "$name <{$values['Email']}>";

	$headers = array(
		'From' => $from,
		'To' => $to,
		'Subject' => $subject
	);
	$smtp = Mail::factory('smtp', array('host' => $host,
				'auth' => true,
				'username' => $username,
				'password' => $password));

	$mail = $smtp->send($to, $headers, $message);

	if (!PEAR::isError($mail)) {
		$response->success = "Mail is successfully sent.";
	} else {
		$response->error .= 'Your mail could not be sent this time.';
	}
}

echo json_encode($response);
exit();
?>